<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ajax</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

    <div id="app">
        <div class="container p-5">
                <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" class="form-control" v-model="name" placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" v-model="email" aria-describedby="emailHelp"
                        placeholder="Enter email">
                </div>
                <button class="btn btn-success btn-sm" v-if="action.value == 'create'" v-on:click="addUser">Submit</button>
                <button class="btn btn-warning btn-sm" v-else="action.value == 'update'" v-on:click="updateUser(action.index, action.id)">Update</button>
        </div>
        <div class="container p-5">
            <table class="table table-bordered table-hover table-sm">
                <thead class="bg-light">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Email</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(user, index) in users" :key="user.id">
                        <th scope="row">@{{++index}}</th>
                        <td>@{{user.name}}</td>
                        <td>@{{user.email}}</td>
                        <td>
                            <button class="btn btn-warning btn-sm" v-on:click="editUser(index, user.id)">edit</button>
                            <button class="btn btn-danger btn-sm" v-on:click="deleteUser(index, user.id)">hapus</button>
                        </td>
                    </tr>
                    <tr>
                </tbody>
            </table>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

    <script>
        new Vue({
            el: '#app',
            data() {
                return {
                    action: {
                        id: null,
                        index: null,
                        value: 'create'
                    },
                    name: '',
                    email: '',
                    users: []
                }
            },
            methods: {
                addUser: function () {
                    let name = this.name.trim();
                    let email = this.email.trim();

                    this.$http.post('api/users', {
                        name: name,
                        email: email
                    }).then(response => {
                        this.users.unshift(
                            {
                                id: response.data.id,
                                name: response.data.name,
                                email: response.data.email
                            }
                        )
                    });
                    this.name = '',
                    this.email = ''
                },
                editUser: function (index, id) {
                    this.$http.get('api/users/' + id).then(response => {
                        let result = response.body.data;
                        this.name = result.name
                        this.email = result.email
                        this.action.id = result.id,
                        this.action.index = index - 1
                        this.action.value = 'result'
                    });
                },
                updateUser: function (index, id) {
                    let name = this.name.trim();
                    let email = this.email.trim();

                    this.$http.put('api/users/' + id, {
                        name: name,
                        email: email
                    }).then(response => {
                        this.users.splice((index), 1,
                            {
                                name: name,
                                email: email
                            }
                        )
                    });
                    this.name = '',
                    this.email = ''
                },
                deleteUser: function (index, id) {
                    this.$http.delete('api/users/' + id).then(response => {
                        this.users.splice((index - 1), 1)
                    });
                }
            },
            mounted() {
                this.$http.get('api/users').then(response => {
                    let result = response.body.data;
                    this.users = result
                });
            },
        })
    </script>
</body>

</html>
