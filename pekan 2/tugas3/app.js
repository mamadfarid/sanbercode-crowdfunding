// Vue.component('comments', {
//     props: ['comment'],
//     template: `
//         <div>
//     `
// })

var app = new Vue({
    el: '#app',
    data: {
        comment: '',
        comments: []
    },
    methods: {
        addComment: function () {
            this.comments.push({
                content: this.comment,
                created_at: new Date().toLocaleString(),
                result: 0
            });
            this.comment = '';
        },
        addVote: function (index) {
            this.comments[index].result == 1 ? this.comments[index].result = 1 : this.comments[index].result += 1
        },
        removeVote: function (index) {
            this.comments[index].result == -1 ? this.comments[index].result = -1 : this.comments[index].result -= 1
        }
    }
});