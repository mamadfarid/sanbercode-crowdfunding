var app = new Vue({
    el: '#app',
    data() {
        return {
            action: {
                id: null,
                action: 'store'
            },
            name: '',
            users: [
                {
                    'name': 'Muhammad Iqbal Mubarok'
                },
                {
                    'name': 'Faqih Muhammad'
                }
            ]
        }
    },
    methods: {
        store: function () {
            this.users.push(
                {
                    'name': this.name
                }
            )
            this.name = ''
        },
        edit: function (index) {
            this.action.id = index
            this.action.action = 'edit'
            this.name = this.users[index].name
        },
        update: function (index) {
            this.users[index].name = this.name
            this.name = ''
        },
        hapus: function (index) {
            alert('hapus?')
            this.users.splice(index, 1)
        }
    },
});