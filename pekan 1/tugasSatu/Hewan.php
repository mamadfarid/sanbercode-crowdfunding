<?php 

class Hewan
{
	protected $nama; 
	protected $darah = 50;
	protected $jumlahKaki;
	protected $keahlian;

	// public function __construct($nama, $darah, $jumlahKaki, $keahlian) {
 //    	$this->nama = $nama;
 //    	$this->darah = $darah;
 //    	$this->jumlahKaki = $jumlahKaki;
 //    	$this->keahlian = $keahlian;
 //  	}
	public function setNama($nama)
	{
		$this->nama = $nama;
	}

	public function getNama()
	{
		return $this->nama;
	}

	public function setDarah($darah)
	{
		$this->darah = $darah;
	}

	public function getDarah()
	{
		return $this->darah;
	}

	public function setJumlahKaki($jumlahKaki)
	{
		$this->jumlahKaki = $jumlahKaki;
	}

	public function getJumlahKaki()
	{
		return $this->jumlahKaki;
	}

	public function setKeahlian($keahlian)
	{
		$this->keahlian = $keahlian;
	}

	public function getKeahlian()
	{
		return $this->keahlian;
	}

	public function atraksi()
	{
		echo $this->nama . " " . $this->keahlian;
	}
}