<?php

class Harimau extends Hewan
{
	public function getInfoHewan()
	{
		parent::setNama("Harimau");
		parent::setJumlahKaki("4");
		parent::setKeahlian("Berlari");

		echo "Nama Hewan : " . parent::getNama() . "<br>";
		echo "Darah Hewan : " . parent::getDarah() . "<br>";
		echo "Jumlah Kaki : " . parent::getJumlahKaki() . "<br>";
		echo "Keahlian : " . parent::getKeahlian() . "<br>";
	}
}

