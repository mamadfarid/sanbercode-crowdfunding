<?php 

require("Hewan.php");

class Elang extends Hewan
{
	public function getInfoHewan()
	{
		parent::setNama("Elang");
		parent::setJumlahKaki("2");
		parent::setKeahlian("Terbang");

		echo "Nama Hewan : " . parent::getNama() . "<br>";
		echo "Darah Hewan : " . parent::getDarah() . "<br>";
		echo "Jumlah Kaki : " . parent::getJumlahKaki() . "<br>";
		echo "Keahlian : " . parent::getKeahlian() . "<br>";
	}
}

